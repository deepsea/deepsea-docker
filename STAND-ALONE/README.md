# Docker image for DEEPSEA-in-a-container

The major benefit is that it obviates the need to install Z3 and set up the
Java classpaths and libraries.

**You do not have to produce this image yourself if you merely want to run
DEEPSEA in a container.  It is available on ``cloud.docker.com``.**

## Requirements

The scripts assume the following:

- Unix environment
- ``git``
- ``docker``

## How to

- To create the image, run

        $ make

- To use the image, run

        $ docker run --interactive --tty --volume "$PWD":/work deepsea-docker:latest my.properties

