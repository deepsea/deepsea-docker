# Create Docker image: ``deepsea-logger``

Used for remote logging in the swarm.

**You do not have to produce this image yourself if you merely want to run
DEEPSEA in a container.  It is available on ``cloud.docker.com``.**

## Requirements

The scripts assume the following:

- Unix environment
- ``git``
- ``maven``
- ``docker``

## How to

- To create the image, run

        $ make

- To start the container, run

        $ docker run --interactive --tty --volume=`pwd`/logging:/logging deepsea-logger:latest

  Note that you should not need to start the container in this way; it is
  usually part of a docker composition.

