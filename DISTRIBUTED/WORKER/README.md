# Create Docker image: ``deepsea-worker``

Used for worker in the swarm.

**You do not have to produce this image yourself if you merely want to run
DEEPSEA in a container.  It is available on ``cloud.docker.com``.**

## Requirements

The scripts assume the following:

- Unix environment
- ``git``
- ``docker``

## How to

- To create the image, run

        $ make

- To start the container, run

        $ docker run ...

  Note that you should not need to start the container in this way; it is
  usually part of a docker composition.

