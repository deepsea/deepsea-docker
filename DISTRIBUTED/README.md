# Docker image for DEEPSEA-in-a-swarm

Scripts for running DEEPSEA in a Docker swarm.

Scripts to build various components can be found in the subdirectories
``logger``, ``master``, and ``worker``.

