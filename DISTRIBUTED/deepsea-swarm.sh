#!/bin/sh
# Deploy a stack of deepsea workers and one master

MYPATH="`dirname \"$0\"`"
MYPATH="`( cd \"$MYPATH\" && pwd )`"
export ARGS="${@}"

docker-machine ssh myvm1 "mkdir ./data 2>/dev/null" 2>/dev/null
docker-machine ssh myvm1 "mkdir ./logging 2>/dev/null" 2>/dev/null
eval $(docker-machine env myvm1)
docker stack deploy --compose-file ${MYPATH}/docker-compose.yml deepsea
