#!/bin/sh
cp /libz3*so /lib/
export LD_LIBRARY_PATH=/
/usr/bin/java -Djava.library.path=/ -classpath .:work:${CLASSPATH} -Dlog4j.configurationFile=/log4j2-master2.xml za.ac.sun.cs.deepsea.distributed.Master ${@}
